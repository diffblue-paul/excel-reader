package uk.co.testforce.framework.properties;

import uk.co.testforce.framework.excel.TestData;

public class ExecutionContext {
    private static ExecutionContext singleton = null;
    private ConfigData configData = null;
    private String runID = null;
    private boolean reportingFromSuite = true;
    private TestData testData = null;

    private ExecutionContext() {
    }

    public static ExecutionContext getInstance() {
        if (singleton == null) {
            singleton = new ExecutionContext();
        }

        return singleton;
    }

    public ConfigData getConfigData() {
        return this.configData;
    }

    public void setConfigData(ConfigData configData) {
        this.configData = configData;
    }

    public String getRunID() {
        return this.runID;
    }

    public void setRunID(String runID) {
        this.runID = runID;
    }

    public TestData getTestData() {
        return this.testData;
    }

    public void setTestData(TestData testData) {
        this.testData = testData;
    }
}

