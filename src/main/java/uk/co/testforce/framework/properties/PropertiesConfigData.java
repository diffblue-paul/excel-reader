package uk.co.testforce.framework.properties;

import uk.co.testforce.framework.utils.StringUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class PropertiesConfigData extends ConfigData {
    protected Properties configData;

    public PropertiesConfigData(List<String> filepaths) {
        super(filepaths);
    }

    protected void loadConfigData(String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("PropertiesConfigData: loadConfigData(String): error: filePath argument cannot be null");
        } else {
            Object is = this.getClass().getClassLoader().getResourceAsStream(filePath);
            if (is == null) {
                try {
                    is = new FileInputStream(filePath);
                } catch (FileNotFoundException var5) {
                    throw new RuntimeException("PropertiesConfigData: loadConfigData(String): error: cannot find " + filePath);
                }
            }

            try {
                if (this.configData == null) {
                    this.configData = new Properties();
                }

                if (filePath.endsWith(".xml")) {
                    this.configData.loadFromXML((InputStream) is);
                } else {
                    this.configData.load((InputStream) is);
                }

            } catch (IOException var4) {
                throw new RuntimeException("PropertiesConfigData: loadConfigData(String): error: cannot read " + filePath, var4);
            }
        }
    }

    public Collection<String> getKeys() {
        HashSet result = new HashSet();
        Iterator var2 = this.configData.keySet().iterator();

        while (var2.hasNext()) {
            Object o = var2.next();
            result.add((String) o);
        }

        return result;
    }

    public final String getProperty(String key, String... subkeys) {
        return this.configData.getProperty(StringUtils.concatWith(this.KEYWORD_HIERARCHY_SEPARATOR, key, subkeys));
    }
}

