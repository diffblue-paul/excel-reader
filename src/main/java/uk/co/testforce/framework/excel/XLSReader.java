package uk.co.testforce.framework.excel;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XLSReader {
    private static final int COLUMN_START = 0;
    private static final int ROW_HEADERS = 0;
    private File file = null;

    public XLSReader(File file) {
        this.file = file;
    }

    public void load(String dataSheetName, Map<String, String> dataMap, String key, String value) {
        try {
            Workbook testDataWorkbook = WorkbookFactory.create(this.file);
            HSSFFormulaEvaluator.evaluateAllFormulaCells(testDataWorkbook);
            this.load(testDataWorkbook, dataSheetName, dataMap, key, value);
            this.load(testDataWorkbook, dataSheetName, dataMap, key, value);
            testDataWorkbook.close();
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    protected Map<String, String> getRowInfo(Sheet testDataSheet, Row headers, int row) {
        Map<String, String> rowMap = new HashMap();
        Cell headerCell = null;
        String header = null;
        Row data = testDataSheet.getRow(row);

        for (int p = 0; (headerCell = headers.getCell(p)) != null && (header = this.getCellValueAsString(headerCell)) != null && !header.equals(""); ++p) {
            try {
                data.getCell(p);
            } catch (Exception var11) {
                rowMap.put(header, "");
                continue;
            }

            Cell datumCell = data.getCell(p);
            String datum;
            if (datumCell == null || (datum = this.getCellValueAsString(datumCell)) == null) {
                datum = "";
            }

            rowMap.put(header, datum);
        }

        return rowMap;
    }

    public void load(Workbook testDataWorkbook, String dataSheetName, Map<String, String> dataMap, String key, String value) {
        Sheet testDataSheet = testDataWorkbook.getSheet(dataSheetName);
        Row headers = testDataSheet.getRow(0);
        int lastRow = testDataSheet.getLastRowNum();
        Map<String, String> lastRowMap = null;

        for (int row = 1; row <= lastRow; ++row) {
            lastRowMap = this.getRowInfo(testDataSheet, headers, row);
            if (lastRowMap != null && (key == null || lastRowMap.containsKey(key) && ((String) lastRowMap.get(key)).equals(value))) {
                dataMap.putAll(lastRowMap);
                return;
            }
        }

        dataMap.putAll(lastRowMap);
    }

    private String getCellValueAsString(Cell cell) {
        //cell.setCellType(1);
        DataFormatter formatter = new DataFormatter();
        String cellValue = formatter.formatCellValue(cell);
        return cellValue;
    }

    public void loadMultipleLines(String dataSheetName, Map<String, String> dataMap, String key, String value) {
        try {
            Workbook testDataWorkbook = WorkbookFactory.create(this.file);
            HSSFFormulaEvaluator.evaluateAllFormulaCells(testDataWorkbook);
            this.loadLines(testDataWorkbook, dataSheetName, dataMap, key, value);
            this.loadLines(testDataWorkbook, dataSheetName, dataMap, key, value);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public void loadLines(Workbook testDataWorkbook, String dataSheetName, Map<String, String> dataMap, String key, String value) {
        Sheet testDataSheet = testDataWorkbook.getSheet(dataSheetName);
        Row headers = testDataSheet.getRow(0);
        int lastRow = testDataSheet.getLastRowNum();
        Map<String, String> lastRowMap = null;

        for (int row = 1; row <= lastRow; ++row) {
            lastRowMap = this.getRowInfo(testDataSheet, headers, row);
            if (lastRowMap != null && (key == null || lastRowMap.containsKey(key) && ((String) lastRowMap.get(key)).equals(value))) {
                dataMap.putAll(this.transformToMultiLineTestData(lastRowMap, row));
            }
        }

    }

    private Map<String, String> transformToMultiLineTestData(Map<String, String> lastRowMap, int row) {
        Map<String, String> returnValue = new HashMap();
        Iterator i$ = lastRowMap.keySet().iterator();

        while (i$.hasNext()) {
            String key = (String) i$.next();
            String value = (String) lastRowMap.get(key);
            returnValue.put(key + "" + row, value);
        }

        return returnValue;
    }
}

